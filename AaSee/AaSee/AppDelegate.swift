//
//  AppDelegate.swift
//  AaSee
//
//  Created by apple on 06/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UITabBarControllerDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var navigationController:UINavigationController?
    let tabBarController = UITabBarController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = .red
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes  =  [NSAttributedString.Key.foregroundColor:UIColor.white]
      registerForPushNotifications()
        if UserDefaults.standard.bool(forKey: "firstTime") {
         openHome()
        }
       
        return true
    }

    func registerForPushNotifications() {
        UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
                granted, error in
                print("Permission granted: \(granted)") // 3
        }
        self.getNotificationSettings()
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
        }
    }

    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
        ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
    }
    
    
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func openHome(){
        
        let storyBoard  = UIStoryboard(name: "Main", bundle: nil)
        
         let vc1 = storyBoard.instantiateViewController(withIdentifier: "homeViewController") as! HomeViewController
        let nv1 = UINavigationController(rootViewController: vc1)
        // nv1.restorationIdentifier = "gigListViewController"
        
        var nv2 : UINavigationController!
        
        let vc2 = storyBoard.instantiateViewController(withIdentifier: "shareViewController") as! ShareViewController
        nv2 = UINavigationController(rootViewController: vc2)
        //  nv2.restorationIdentifier = "myPostedViewController"
        
        
        let vc3 = storyBoard.instantiateViewController(withIdentifier: "rateViewController") as! RateViewController
        let nv3 = UINavigationController(rootViewController: vc3)
        
        //  nv3.restorationIdentifier = "communityViewController"
        
        let vc4 = storyBoard.instantiateViewController(withIdentifier: "contactViewController") as! ContactViewController
        let nv4 = UINavigationController(rootViewController: vc4)
        //   nv4.restorationIdentifier = "chatsListViewController"
        
        
        
        let vc5 = storyBoard.instantiateViewController(withIdentifier: "aboutUsViewController") as! AboutUsViewController
        let nv5 = UINavigationController(rootViewController: vc5)
        
        nv1.tabBarItem = UITabBarItem.init(title: "", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
        nv2.tabBarItem = UITabBarItem.init(title: "", image: UIImage(named: "share"), selectedImage: UIImage(named: "share"))
        nv3.tabBarItem = UITabBarItem.init(title: "", image: UIImage(named: "rating"), selectedImage: UIImage(named: "rating"))
        
        nv4.tabBarItem = UITabBarItem.init(title: "", image: UIImage(named: "contact"), selectedImage: UIImage(named: "contact"))
        
         nv5.tabBarItem = UITabBarItem.init(title: "", image: UIImage(named: "contact"), selectedImage: UIImage(named: "contact"))
        
        
        nv1.tabBarItem.title = ""
        nv2.tabBarItem.title = ""
        nv3.tabBarItem.title = ""
        nv4.tabBarItem.title = ""
        nv5.tabBarItem.title = ""
       
        
        var bottomSpace : CGFloat = -10
        var topSpace : CGFloat = 0
        
        if #available(iOS 11.0, *) {
            bottomSpace  = -10
            topSpace = 0
        }else {
            bottomSpace  = -5
            topSpace = 5
        }
        
        nv1.tabBarItem.imageInsets = UIEdgeInsets(top: topSpace, left: 0, bottom: bottomSpace, right: 0)
        nv2.tabBarItem.imageInsets = UIEdgeInsets(top: topSpace, left: 0, bottom: bottomSpace, right: 0)
        nv3.tabBarItem.imageInsets = UIEdgeInsets(top: topSpace, left: 0, bottom: bottomSpace, right: 0)
        nv4.tabBarItem.imageInsets = UIEdgeInsets(top: topSpace, left: 0, bottom: bottomSpace, right: 0)
       nv5.tabBarItem.imageInsets = UIEdgeInsets(top: topSpace, left: 0, bottom: bottomSpace, right: 0)

        tabBarController.tabBar.shadowImage = nil
        tabBarController.delegate = self
        tabBarController.viewControllers = [nv1, nv2, nv3, nv4]
        tabBarController.tabBar.tintColor = UIColor.red//CustomColor.appThemeColor
        tabBarController.selectedIndex = 0
        
        if self.navigationController == nil {
           appDelegate.window?.rootViewController = tabBarController
        }else {
            self.navigationController?.present(tabBarController, animated: true, completion: {
                
            })
        }
    }
    
 
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

