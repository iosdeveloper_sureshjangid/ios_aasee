//
//  BaseViewController.swift
//  Panther
//
//  Created by Pankaj Jangid on 1/03/19.
//  Copyright © 2017 Manish Jangid. All rights reserved.
//

import UIKit


import Toast

import SVProgressHUD


class BaseViewController: UIViewController {
    
    fileprivate var mScrollView : UIScrollView? = nil
    
    var navTitle: UILabel!
  
    let leftButton = UIButton()
    
    var ischeckforFilter = false
    var tapGesture: UITapGestureRecognizer!

    let bgImage : UIImageView = UIImageView()
    var isAddressTableView:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = .white
      
        
        leftButton.frame =  CGRect(x: -30, y: 0, width: 40, height: 40)
        leftButton.addTarget(self, action: #selector(menuClicked), for: UIControl.Event.touchUpInside)
        leftButton.contentMode = .scaleAspectFit
        leftButton.backgroundColor = .clear
        leftButton.setImage(UIImage(named: "back_arrow_nav"), for: UIControl.State.normal)
        leftButton.imageEdgeInsets = UIEdgeInsets(top: 12, left: 2, bottom: 11, right:10)
        
        navigationBarAppearace.isTranslucent = true
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]

        navigationController?.navigationItem.backBarButtonItem?.title = ""
        navigationController?.navigationItem.backBarButtonItem?.title = nil
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.gray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
        //self.navigationItem.backBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        
        // Do any additional setup after loading the view.
       
        bgImage.frame =  UIScreen.main.bounds
        bgImage.contentMode = .scaleAspectFill
        bgImage.clipsToBounds = true
        self.view.addSubview(bgImage)
        self.view.sendSubviewToBack(bgImage)
        
        
      
      
    
        navTitle = UILabel()
         navTitle.text = "AaySee"
        navTitle.font = UIFont.boldSystemFont(ofSize: 20.0)
        navTitle.textColor = .white
        navTitle.frame =  CGRect(x: 12, y: 0, width:(UIScreen.main.bounds.size.width/2)+80.0, height: 44) //CGRect(x: 00, y: 0, width: ((UIScreen.main.bounds.size.width/2)+40.0), height: 40)//
        navTitle.numberOfLines = 1
        navTitle.sizeToFit()
        //  navTitle.backgroundColor = .green
        navTitle.lineBreakMode = .byWordWrapping
        
        
        let leftItem = UIBarButtonItem(customView: navTitle)
       //self.navigationItem.leftBarButtonItem = leftItem
        
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: leftButton),leftItem]
    }
    
    
        
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
    }
    

    @objc func menuClicked () {
        
             self.navigationController?.popViewController(animated: true)
    }
    
    func isShowMenuIcon() -> Bool {
        return true
    }
    func backBtnTapped() {
     //    appDelegate.tabBarController.tabBar.isHidden = false

        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        
    
    }
    
  
    func makeToast(_ txt: String){
        self.view.hideToasts()
        CSToastManager.sharedStyle()?.backgroundColor = CustomColor.appThemeColor
        self.view.makeToast(txt, duration: 1.5, position: "CSToastPositionCenter")
        
        
    }
  
 
    
    //hud show and hide
    
    func hudShow()  {
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    func hudHide()  {
        SVProgressHUD.dismiss()
    }
//


}
