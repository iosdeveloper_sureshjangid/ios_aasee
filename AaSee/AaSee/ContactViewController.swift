//
//  ContactViewController.swift
//  AaSee
//
//  Created by apple on 10/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import WebKit

class ContactViewController: BaseViewController,WKNavigationDelegate,WKUIDelegate,UIScrollViewDelegate {

    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        view = webView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.hudShow()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let myURL = URL(string:"https://www.aaysee.com/Contact.aspx")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        webView.scrollView.bounces = false
        webView.contentMode = .scaleAspectFit
        self.leftButton.isHidden = true
        //        webView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        //
        //        webView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        //
        //        webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        //
        //        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        //
        //        //view = webView
        //        self.view.addSubview(webView)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished navigating to url \(webView.url)")
       // self.hudHide()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        //[webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, webView.scrollView.contentSize.height)];
        webView.scrollView.contentSize = CGSize(width: view.frame.size.width, height: webView.scrollView.contentSize.height)
        self.webView.scrollView.showsHorizontalScrollIndicator = false
    }
    
    //    func webViewDidFinishLoad(_ webView: UIWebView) {
    //        self.webView.scrollView.showsHorizontalScrollIndicator = false
    //    }
    //
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.x > 0){
            scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
        }
    }
    
}
