//
//  ContactUsViewController.swift
//  GigQwik // remove this line
//
//  Created by Sourabh Sharma on 12/2/17.
//  Copyright © 2018 KrishMac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ContactUsViewController: BaseViewController,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
  
    @IBOutlet weak var contactNumberField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navTitle.text = "Contact Us"
        self.leftButton.isHidden = true
        submitButton.layer.cornerRadius = submitButton.layer.frame.height/2
        submitButton.clipsToBounds = true
        emailTextField.delegate = self
        nameTextField.delegate = self
        messageTextView.delegate = self
      contactNumberField.delegate = self
        contactNumberField.textAlignment = .center
        emailTextField.textAlignment = .center
        nameTextField.textAlignment = .center
        IQKeyboardManager.shared.enable = true
        emailTextField.placeholder = "Email *"
     //   emailTextField.title = "Subject *"
        contactNumberField.keyboardType = .numberPad
     
        nameTextField.placeholder = "Your Name"
       // nameTextField.title = "Email *"
       // self.leftButton.isHidden = true

        messageTextView.font = UIFont.systemFont(ofSize: 12.0)
        messageTextView.text =  "Message *"
        if messageTextView.text == "Message *" {
            messageTextView.textColor = UIColor.black
        } else {
            messageTextView.textColor = UIColor.black
        }
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView.text.characters.count == 0 {
            textView.text = "Message *"
            self.lineView.backgroundColor =  UIColor.gray
            textView.resignFirstResponder()
        }
        if messageTextView.text == "Message *" {
            messageTextView.textColor =  UIColor.gray
            self.lineView.backgroundColor =  UIColor.gray
        } else {
            self.lineView.backgroundColor =  UIColor.gray
            messageTextView.textColor = UIColor.black
        }
       
        return true
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Message *" {
            self.messageTextView.text = ""
        }
        if messageTextView.text == "Message *" {
            self.lineView.backgroundColor =  .red
            messageTextView.textColor =  UIColor.black
        } else {
            self.lineView.backgroundColor = UIColor.gray
            messageTextView.textColor = UIColor.black
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            if messageTextView.text == "Message *" {
                self.lineView.backgroundColor = .red
                messageTextView.textColor =  UIColor.gray
            } else {
                self.lineView.backgroundColor = .red
                messageTextView.textColor = UIColor.black
            }
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func submitClicked(_ sender: Any) {
        if checkValidation() {
        self.delay(1.5) {
            self.makeToast("Data Saved Successfully")
        }
        }
    }
    func checkValidation() -> Bool {
        
        if(nameTextField.text!.isBlank)
        {
           self.makeToast("Name is required")
                        return false
        }
        if(emailTextField.text!.isBlank)
        {
            self.makeToast("Email is required")
            
            return false
        }
        
        if(!emailTextField.text!.isEmail)
        {
//            nameTextField.errorColor = CustomColor.redColor
//            nameTextField.errorMessage = err_valid_email
          self.makeToast("Email is Invalid")
            return false
        }
        
        if(contactNumberField.text!.isBlank)
        {
            self.makeToast("Contact Number is required")
            
            return false
        }
      
        if(messageTextView.text!.isBlank)
        {
         self.makeToast("Enter message")
            return false
        }
        return true
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    
}
