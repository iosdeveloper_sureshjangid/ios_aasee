//
//  HomeViewController.swift
//  AaSee
//
//  Created by apple on 06/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import WebKit

class HomeViewController: BaseViewController,WKNavigationDelegate,WKUIDelegate,UIScrollViewDelegate {
    
    //@IBOutlet weak var webView: WKWebView!
    //  var webView: WKWebView!
    var webView: WKWebView!
    var list_arr : [String]!
    var backList: [String] = []
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        list_arr = [String]()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        view = webView
  
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
              leftButton.isHidden = true
        loadWebViewPage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //https://www.aaysee.com/Login_Form.aspx
    
    func loadWebViewPage() {
        let myURL = URL(string:"https://www.aaysee.com/")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        webView.scrollView.bounces = false
        webView.contentMode = .scaleAspectFit
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
      //  self.hudShow()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        self.hudHide()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //print("Finished navigating to url \(webView.url)")
//        self.hudHide()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        webView.scrollView.contentSize = CGSize(width: view.frame.size.width, height: webView.scrollView.contentSize.height)
        self.webView.scrollView.showsHorizontalScrollIndicator = false
        
        if backList.count > 1 {
            self.leftButton.isHidden = false
        } else {
             self.leftButton.isHidden = true
        }

    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        
        if let urlStr = navigationAction.request.url?.absoluteString{
            //urlStr is what you want, I guess.
            if backList.last != urlStr {
              backList.append(urlStr)
            }
        }
        decisionHandler(.allow)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.x > 0){
            scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
        }
    }
    
    override func menuClicked() {
        
        if backList.count > 1 {
            
            let myURL = URL(string:backList[backList.count - 2])
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
           backList.removeLast()
        } else {
            
         self.leftButton.isHidden = true
    }
    
    }
}


extension HomeViewController : UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex == 0 {
            print(backList)
            backList.removeAll()
            loadWebViewPage()
            print("qwer")
            self.leftButton.isHidden = true

        }
        
    }
    
}
