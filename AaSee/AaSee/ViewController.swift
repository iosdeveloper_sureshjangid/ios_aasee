//
//  ViewController.swift
//  AaSee
//
//  Created by apple on 06/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func homeButtonClicked(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "firstTime")
        appDelegate.openHome()
    }
    
}

