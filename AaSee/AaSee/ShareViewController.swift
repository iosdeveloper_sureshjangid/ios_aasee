//
//  ShareViewController.swift
//  AaSee
//
//  Created by apple on 06/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ShareViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
self.leftButton.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_tap_share(_ sender: Any) {
        let someText:String = "Click here to download AaySee App"
        let appUrl = "https://itunes.apple.com/in/app/AaySee/id1453979933?mt=8"
        
  //      https://itunes.apple.com/us/app/kidsmart/id1320743814?ls=1&mt=8
        let objectsToShare:URL = URL(string: appUrl)!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail,UIActivity.ActivityType.postToFlickr]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
