//
//  AboutUsViewController.swift
//  AaSee
//
//  Created by apple on 12/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController {
    
    
    @IBOutlet weak var lbl_detail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
self.leftButton.isHidden = true
        // Do any additional setup after loading the view.
        let str  = """
    Our philosophy of putting the customer first has guided us to create a shopping platform that enhances the quality of life, stimulate local economic activity and contribute to community life and culture. We believe in keeping the customers happy and providing them with products at a very competent price.

    AAYSEE MALL PRIVATE LIMITED is an online shopping mall where people can fulfill their daily needs while sitting at home or at work. We are here to make people’s life easy and add some more time to their personal life.
        
    AAYSEE MALL PRIVATE LIMITED deals in its own products like Men and Women garments, footwear and ALSO empowers restaurants, grocery and Farm based product (Fruits & Vegetables) businesses to reach millions of customers with numerous programs that helps to boost their revenue, customer base and productivity. We also target to give a minimum assured business to our vendors and business partners every month.
        
    With its Registered office at Ahmedabad (Address: 2nd Floor, Block N, Safal Mondeal Retail Park, Bodakdev, Rajpath Club, S.G. Road, Ahmedabad, Gujrat 380054), AAYSEE MALL PRIVATE LIMITED also has presence in other cities like Mumbai, Bangalore, Hyderabad, Kolkata, Jaipur, Visakhapatnam and Chennai. Our Garment Exports touch 3 different countries of the world i.e. Spain, Australia and South Africa.
 """
        
lbl_detail.text = str
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
